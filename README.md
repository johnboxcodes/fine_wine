## World wines 🍷

This is an assessment for Nook.


Check it online 👉 [here](https://wine.johnboxcodes.com)

![Image of Yaktocat](./finewine.png)

## Run ⚙️

### Dev server
```npm run start```

### Build
```npm run build```


# Nook Frontend Coding Challenge

In all cases clone the project, create a branch in your name and create a pull request for review

## 1. Wine Shop

Complete the following tasks to complete the included design

### Data Manipulation 1

Using the pre-existing all filter and add filters France and Portugal using `origins` key (We are testing data manipulation and the ability to use existent components)

### Data Manipulation 2

Implement search using keys `product_name`, `_keywords` and `brands`

### CSS

Implement the card component as per the included design example
