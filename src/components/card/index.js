import React, { useState } from "react";

import {
  Container,
  CardWineThumb,
  CardWineTitle,
  CardWineCountry,
  CardWineInfoContainer,
  CardWineType,
  CardWineFactsContainer,
  CardWineThumbContainer,
  CardWineTitleCountryContainer,
  CardWineYear,
  CardWineAlcohol,
  CardWineThumbSkeleton
} from "./styles";

const Card = ({ wine }) => {
  // doing like this because there doens't seem to have this info on the
  // available data. it may generate ui updating though...
  // also, it will not always match the API keyword of wine type
  const mockWineType = () => {
    const wines = ['Red', 'Rose', 'White'];
    const typeIndex = Math.floor(Math.random() * 3);
    return wines[typeIndex];
  }
  const [imgLoaded, setImgLoaded] = useState(false);
  const year = (wine) => wine.entry_dates_tags[wine.entry_dates_tags.length - 1]
  return (
    <Container data-testid="card-container">
      <CardWineInfoContainer>
        <CardWineThumbContainer>
          {!imgLoaded && <CardWineThumbSkeleton></CardWineThumbSkeleton>}
          <CardWineThumb src={wine.image_front_thumb_url} alt='Product image'
            onLoad={() => setImgLoaded(true)} />
        </CardWineThumbContainer>
        <CardWineTitleCountryContainer>
          <CardWineTitle>{wine.product_name}</CardWineTitle>
          <CardWineCountry>{wine.countries.replace('en:', '')}</CardWineCountry>
        </CardWineTitleCountryContainer>
      </CardWineInfoContainer>
      <CardWineType>{mockWineType()}</CardWineType>
      <CardWineFactsContainer>
        <CardWineAlcohol>Alcohol: {wine.nutriments.alcohol ? wine.nutriments.alcohol + '%'
          : 'N/A'}</CardWineAlcohol>
        <CardWineYear>{year(wine)}</CardWineYear>
      </CardWineFactsContainer>
    </Container>
  );
};
export default Card;
