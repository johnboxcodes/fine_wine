import styled from "styled-components";

import colors from "../../styles/colors";

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items:center;
  justify-content: space-between;
  max-width: 800px;
  position: relative;
  margin-bottom: 8px;
  background-color: ${colors.white};
  padding: 8px;
  border-radius: 12px;
  box-shadow: 2px 2px 8px 0px rgba(0,0,0,0.3);
`;

export const CardWineInfoContainer = styled.div`
display: flex;
margin-right: 16px;
width: 300px;
`
export const CardWineThumbContainer = styled.div`
margin-right: 16px;
`
export const CardWineTitleCountryContainer = styled.div`
display: flex;
flex-direction: column;
`

export const CardWineFactsContainer = styled.div`
margin-right: 16px;
width: 200px;
`
export const CardWineThumb = styled.img`
  border-radius: 50%;
  width: 42px;
  height: 42px;
`;
export const CardWineThumbSkeleton = styled.div`
  border-radius: 50%;
  width: 42px;
  height: 42px;
  background-color: ${colors.grey2};
`;
export const CardWineTitle = styled.span`
  font-weight: bold;
  display: block;
  margin-bottom: 4px;
`;

export const CardWineAlcohol = styled.span`
  font-weight: bold;
  display: block;
  margin-bottom: 4px;
`;

export const CardWineYear = styled.span`
  font-weight: bold;
  display: block;
  margin-bottom: 4px;
  color: ${colors.grey1};
`;

export const CardWineCountry = styled.span`
  color: ${colors.grey1};
  display: block;
  font-weight: bold;
  font-size: 14px;
`;

export const CardWineType = styled.div`
  color: ${colors.grey1};
  padding: 8px;
  border-radius: 16px;
  text-align: center;
  width: 75px;
  background-color: ${colors.grey2};
  display: block;
  font-weight: 700;
  font-size: 14px;
  margin-right: 16px;
`;

