import React from "react";

import { Container, PreviousPage, NextPage } from "./styles";

const Pagination = ({
  previousPage,
  nextPage,
  canPreviousPage,
  canNextPage,
  pageIndex,
  pageCount,
  pageSize,
  setPageSize,
}) => {
  return (
    <Container>
      <div>
        <PreviousPage
          onClick={() => previousPage()}
          disabled={!canPreviousPage}
          size={16}
        />
        <NextPage
          onClick={() => nextPage()}
          disabled={!canNextPage}
          size={16}
        />
        <span>
          Page {pageCount ? pageIndex + 1 : 0} of {pageCount}
        </span>
      </div>
      <div>
        <select
          value={pageSize}
          onChange={(e) => {
            setPageSize(Number(e.target.value));
          }}
        >
          {[10, 25, 50, 75, 100].map((pageSize) => (
            <option key={pageSize} value={pageSize}>
              Show {pageSize}
            </option>
          ))}
        </select>
      </div>
    </Container>
  );
};

export default Pagination;
