import styled, { css } from "styled-components";
import { HiOutlineChevronLeft, HiOutlineChevronRight } from "react-icons/hi";

import colors from "../../styles/colors";

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  width: 100%;
  min-height: 36px;

  color: ${colors.grey1};

  div {
    display: flex;
    align-items: center;
  }

  select {
    border: none;
    background: transparent;
    color: ${colors.grey1};
  }
`;

export const PreviousPage = styled(HiOutlineChevronLeft)`
  margin-right: 10px;
  cursor: pointer;
  ${(props) =>
    props.disabled &&
    css`
      color: ${colors.grey2};
      cursor: default;
    `}
`;

export const NextPage = styled(HiOutlineChevronRight)`
  margin-right: 10px;
  cursor: pointer;
  ${(props) =>
    props.disabled &&
    css`
      color: ${colors.grey2};
      cursor: default;
    `}
`;
