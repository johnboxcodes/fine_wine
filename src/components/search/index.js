import React, { useState, useRef } from "react";
import { FaSearch } from "react-icons/fa";

import {
  Container,
  Input,
  InputWrapper,
  SelectBy,
  SearchOptions,
  SelectLabel,
  SelectWrapper
} from "./styles";
import PropTypes from 'prop-types';

const Search = ({ setText, setSearchKey, ...rest }) => {
  const [focus, setFocus] = useState(false)
  const inputRef = useRef()
  const handleSelectChange = (event) => {
    // inputRef.current.value = null;
    const value = event.target.value;
    setSearchKey(value);
  };
  return (
    <Container focus={focus} data-testid="search-container">
      <InputWrapper>
        <Input
          ref={inputRef}
          placeholder="Search"
          onChange={e => setText(e.target.value)}
          onFocus={() => setFocus(true)}
          onBlur={() => setFocus(false)}
          focus={focus}
          {...rest}
        />
        <FaSearch />
      </InputWrapper>
      <SelectWrapper>
        <SelectLabel>Search by</SelectLabel>
        <SelectBy defaultValue={'product_name'} onChange={(event) => handleSelectChange(event)}  >
          <SearchOptions value='product_name'>Product name</SearchOptions>
          <SearchOptions value='_keywords'>Keywords</SearchOptions>
          <SearchOptions value='brands'>Brands</SearchOptions>
        </SelectBy>
      </SelectWrapper>
    </Container>
  );
};
Search.propTypes = {
  setText: PropTypes.func,
  setSearchKey: PropTypes.func
}
export default Search;
