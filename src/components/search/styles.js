import styled from "styled-components";

import colors from "../../styles/colors";

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  max-width: 500px;
  
`;
export const InputWrapper = styled.div`
margin-top: 32px;
  position: relative;
  > svg {
    fill: ${colors.grey1};
    position: absolute;
    top: 18px;
    right: 20px;
  }
`

export const Input = styled.input`
  height: 50px;
  line-height: 25px;
  max-width: 400px;
  flex: 1;
  background: ${colors.white};
  border-radius: 25px;
  border: 1px solid
    ${(props) => (props.focus ? colors.secondary : colors.white2)};

  padding: 0 40px 0 20px;
  font-size: 16px;
  color: ${colors.grey1};

  &::placeholder {
    font-size: 16px;
    color: ${colors.grey1};
  }
`;

export const SelectWrapper = styled.div`
margin: 8px 32px;`
export const SelectBy = styled.select`
  cursor: pointer;
  background-color: ${colors.white};
  border: none;
  border-radius: 15px;
  padding: 8px;
  margin: 0;
  width: 180px;
`
export const SelectLabel = styled.div`
font-size: 16px;
font-weight: bold;
margin-bottom: 16px;
text-align: center;
`
export const SearchOptions = styled.option`
  &:checked {
    background-color: ${colors.grey1}
  }

`
