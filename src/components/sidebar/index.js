import React from "react";
import { useHistory } from "react-router";

import Logo from "../../resources/images/logo.png";
import { Aside, Header, Content, ItemContainer, Text } from "./styles";

const Sidebar = () => {
  const history = useHistory();

  const verifyPathname = (pathname) => {
    const actualPathname = window.location.pathname;
    return pathname === actualPathname;
  };

  const changePage = (pathname) => {
    history.push(pathname);
  };

  return (
    <Aside>
      <Header>
        <img
          data-testid="logo-img"
          src={Logo}
          alt="Nook logo"
          onClick={() => history.push("/")}
        />
      </Header>
      <Content>
        <ItemContainer
          selected={verifyPathname("/")}
          onClick={() => changePage("/")}
        >
          <Text selected={verifyPathname("/")}>Wines</Text>
        </ItemContainer>
      </Content>
    </Aside>
  );
};

export default Sidebar;
