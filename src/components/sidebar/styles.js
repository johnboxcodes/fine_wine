import styled, { css } from "styled-components";

import colors from "../../styles/colors";

export const Aside = styled.aside`
  width: 130px;
  min-width: 130px;
  height: 100vh;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;

  background: ${colors.white};
  box-shadow: 2px 0px 13px -7px rgba(255, 255, 255, 0.0001),
    0px 0px 8px 2px rgba(173, 173, 173, 0.25);
  z-index: 2;
`;

export const Header = styled.div`
  height: 100px;

  display: flex;
  align-items: center;
  justify-content: center;

  margin-bottom: 75px;
  z-index: 2;
  background: ${colors.white};
  img {
    cursor: pointer;
    width: 76px;
  }
`;

export const Content = styled.div`
  flex: 1;
  width: 100%;
`;

export const ItemContainer = styled.div`
  width: 100%;
  min-height: 28px;

  display: flex;
  flex-direction: column;
  justify-content: center;

  margin: 12px 0;
  cursor: pointer;
  &:hover {
    background: ${colors.white2};
  }
  ${(props) =>
    props.selected &&
    css`
      border-left: ${`3px solid ${colors.secondary}`};
    `}
`;

export const Text = styled.label`
  font-family: sans-serif;
  font-weight: bold;
  font-size: 16px;
  line-height: 28px;
  cursor: pointer;
  margin-left: 24px;
  color: ${colors.grey1};
  ${(props) =>
    props.selected &&
    css`
      color: ${colors.secondary};
      margin-left: 21px;
    `}
`;
