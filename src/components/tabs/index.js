import React from "react";

import { Container, Text } from "./styles";

const Tabs = ({ options, selected, setSelected, disabled }) => {
  return (
    <Container>
      {options.map((item) => (
        <Text
          onClick={() => !disabled && setSelected(item.value)}
          selected={selected === item.value}
          key={item.value}
          disabled={disabled}
        >
          {item.label}
        </Text>
      ))}
    </Container>
  );
};

export default Tabs;
