import styled from "styled-components";

import colors from "../../styles/colors";

export const Container = styled.div`
  width: 100%;
  height: 30px;
  display: flex;
  flex-direction: row;
  align-items: center;
  flex-wrap: wrap;
  border-bottom: 2px solid ${colors.grey2};
  margin: 20px 0;
`;

export const Text = styled.label`
  font-family: sans-serif;
  font-weight: bold;
  font-size: 16px;
  line-height: 28px;
  cursor: pointer;
  margin-right: 16px;
  color: ${colors.grey1};
  text-decoration: ${props => props.selected ? 'underline' : 'unset'};
  color: ${props => props.selected ? colors.secondary : colors.grey1};
  text-decoration-color: ${colors.secondary}

`;
