import { useState, useEffect } from "react";
// add key parameter
export function useFilter(value) {

  const [filter, setFilter] = useState();
  const [key, setKey] = useState('product_name');

  const [filteredWines, setFilteredWines] = useState(value);
  useEffect(() => {
    if (filter && value && key) {
      const filteredList = value.filter(item => {
        if (item[key]) {
          if (typeof item[key] === 'string') {
            return item[key].toLowerCase().includes(filter.toLowerCase())
          } else {
            return item[key].join('').includes(filter.toLowerCase())
          }
        }
      })
      setFilteredWines(filteredList)
    }
  }, [filter, value, key]);

  return [filter, setFilter, filteredWines, setKey];
}