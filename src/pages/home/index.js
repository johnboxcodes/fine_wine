import React, { useState, useEffect } from "react";
import { useTable, usePagination } from "react-table";

import Sidebar from "../../components/sidebar";
import Search from "../../components/search";
import Tabs from "../../components/tabs";
import Pagination from "../../components/pagination";
import { PageContainer, PageContent } from "../../styles";
import { HomeTitle, TableContainer, Loading } from "./styles";
import Card from "../../components/card";
import { useFilter } from "../../hooks/useFilter";

const options = [
  { value: "all", label: "All" },
  { value: "france", label: "France" },
  { value: "portugal", label: "Portugal" },
];
const columns = [];
const Home = () => {
  const [wines, setWines] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [searchKey, setSearchKey] = useState('product_names')
  const [filter, setFilter, filteredWines, setKey] = useFilter(wines)
  const {
    state: { pageIndex, pageSize },
    previousPage,
    nextPage,
    setPageSize,
    canPreviousPage,
    canNextPage,
  } = useTable(
    {
      columns,
      data: wines,
      initialState: { pageIndex: 0 },
      manualPagination: true,
      pageCount,
    },
    usePagination
  );

  const loadWines = async (pageIndex, pageSize) => {
    setWines([])
    let url = `https://world.openfoodfacts.org/cgi/search.pl?search_terms=wine&search_simple=1&action=process&json=1?page=${pageIndex + 1
      }&page_size=${pageSize}`
    if (selectedCountry !== 'all') {
      url += `&tagtype_0=origins&tag_contains_0=contains&tag_0=${selectedCountry}`
    }
    const response = await fetch(
      url,
      {
        method: "GET",
      }
    ).then(async (response) => {
      return response.json();
    });
    setPageCount(response.page_count);
    // console.log(response.products)
    setWines(response.products);
  };

  const [selectedCountry, setSelectedCountry] = useState('all')

  const renderList = () => {
    if (filter) {
      return filteredWines.map((wine) => (
        <Card key={wine.code} wine={wine} />
      ))
    } else {
      if (wines.length === 0) {
        return <Loading>Loading List...</Loading>
      }
      return wines.map((wine) => (
        <Card key={wine.code} wine={wine} />
      ))
    }
  }
  useEffect(() => {
    loadWines(pageIndex, pageSize);
  }, [pageIndex, pageSize, selectedCountry]);

  return (
    <PageContainer>
      <Sidebar />
      <PageContent>
        <HomeTitle>Wines of the world</HomeTitle>
        <Search setText={text => setFilter(text)} setSearchKey={searchKey => { setKey(searchKey) }} />
        <Tabs options={options} selected={selectedCountry} setSelected={item => setSelectedCountry(item)} />
        <TableContainer>
          {renderList()}
        </TableContainer>
        <Pagination
          previousPage={previousPage}
          nextPage={nextPage}
          canPreviousPage={canPreviousPage}
          canNextPage={canNextPage}
          pageIndex={pageIndex}
          pageCount={pageCount}
          pageSize={pageSize}
          setPageSize={setPageSize}
        />
      </PageContent>
    </PageContainer>
  );
};

export default Home;
