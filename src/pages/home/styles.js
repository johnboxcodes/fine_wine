import styled from "styled-components";

import { Title } from "../../styles";
import colors from "../../styles/colors";
export const HomeTitle = styled(Title)`
  margin-bottom: 33px;
`;

export const TableContainer = styled.div`
  width: 100%;
  height: calc(100vh - 300px);

  display: flex;
  flex-direction: column;

  overflow-y: auto;
  overflow-x: hidden;
`;

export const Loading = styled.div`
  font-size: 32px;
  padding: 16px;
  margin: auto;
  color: ${colors.secondary};
  font-weight: bold;
`
