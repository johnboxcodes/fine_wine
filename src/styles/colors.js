const colors = {
  primary: "#F2F2F2",
  secondary: "#3498DB",
  white: "#ffffff",
  white2: "#EDEDED",
  black: "#000000",
  grey1: "#94A7B3",
  grey2: "#DFE6E6",
};

export default colors;
