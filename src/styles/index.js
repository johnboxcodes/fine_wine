import styled from "styled-components";

import colors from "../styles/colors";

export const Title = styled.h1`
  font-weight: bold;
  font-size: 48px;
  line-height: 54px;
`;

export const PageContainer = styled.div`
  display: flex;
  flex-direction: row;

  width: 100%;
  height: 100vh;
`;

export const PageContent = styled.div`
  width: 100%;

  display: flex;
  flex-direction: column;

  padding: 33px 39px 20px;
  background: ${colors.primary};
`;
