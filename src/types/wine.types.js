import PropTypes from 'prop-types';

export const winePropTypes = {
  _id: PropTypes.string,
  _keywords: PropTypes.arrayOf(PropTypes.string),
  added_countries_tags: PropTypes.array,
  allergens: PropTypes.string,
  allergens_from_ingredients: PropTypes.string,
  allergens_from_user: PropTypes.string,
  allergens_hierarchy: PropTypes.arrayOf(PropTypes.string),
  allergens_tags: PropTypes.arrayOf(PropTypes.string),
  brands: PropTypes.string,
  brands_tags: PropTypes.arrayOf(PropTypes.string),
  categories: PropTypes.string,
  categories_hierarchy: PropTypes.arrayOf(PropTypes.string),
  categories_lc: PropTypes.string,
  categories_properties: PropTypes.shape({
    "ciqual_food_code:en": PropTypes.string,
    "agribalyse_proxy_food_code:en": PropTypes.string
  }),
  categories_properties_tags: PropTypes.arrayOf(PropTypes.string),
  categories_tags: PropTypes.arrayOf(PropTypes.string),
  checkers_tags: PropTypes.array,
  code: PropTypes.string,
  codes_tags: PropTypes.arrayOf(PropTypes.string),
  compared_to_category: PropTypes.string,
  complete: PropTypes.number,
  completeness: PropTypes.number,
  correctors_tags: PropTypes.arrayOf(PropTypes.string),
  countries: PropTypes.string,
  countries_hierarchy: PropTypes.arrayOf(PropTypes.string),
  countries_tags: PropTypes.arrayOf(PropTypes.string),
  created_t: PropTypes.number,
  creator: PropTypes.string,
  data_quality_bugs_tags: PropTypes.array,
  data_quality_errors_tags: PropTypes.array,
  data_quality_info_tags: PropTypes.arrayOf(PropTypes.string),
  data_quality_tags: PropTypes.arrayOf(PropTypes.string),
  data_quality_warnings_tags: PropTypes.arrayOf(PropTypes.string),
  ecoscore_data: PropTypes.shape({
    adjustments: PropTypes.shape({
      origins_of_ingredients: PropTypes.shape({
        aggregated_origins: PropTypes.arrayOf(
          PropTypes.shape({
            origin: PropTypes.string,
            percent: PropTypes.number
          })
        ),
        epi_score: PropTypes.number,
        epi_value: -PropTypes.number,
        origins_from_origins_field: PropTypes.arrayOf(PropTypes.string),
        transportation_score_be: PropTypes.number,
        transportation_score_ch: PropTypes.number,
        transportation_score_de: PropTypes.number,
        transportation_score_es: PropTypes.number,
        transportation_score_fr: PropTypes.number,
        transportation_score_ie: PropTypes.number,
        transportation_score_it: PropTypes.number,
        transportation_score_lu: PropTypes.number,
        transportation_score_nl: PropTypes.number,
        transportation_score_uk: PropTypes.number,
        transportation_value_be: PropTypes.number,
        transportation_value_ch: PropTypes.number,
        transportation_value_de: PropTypes.number,
        transportation_value_es: PropTypes.number,
        transportation_value_fr: PropTypes.number,
        transportation_value_ie: PropTypes.number,
        transportation_value_it: PropTypes.number,
        transportation_value_lu: PropTypes.number,
        transportation_value_nl: PropTypes.number,
        transportation_value_uk: PropTypes.number,
        value_be: -PropTypes.number,
        value_ch: -PropTypes.number,
        value_de: -PropTypes.number,
        value_es: -PropTypes.number,
        value_fr: -PropTypes.number,
        value_ie: -PropTypes.number,
        value_it: -PropTypes.number,
        value_lu: -PropTypes.number,
        value_nl: -PropTypes.number,
        value_uk: -PropTypes.number,
        warning: PropTypes.string
      }),
      packaging: PropTypes.shape({
        non_recyclable_and_non_biodegradable_materials: PropTypes.number,
        value: -PropTypes.number,
        warning: PropTypes.string,
        packagings: PropTypes.arrayOf(
          PropTypes.shape({
            ecoscore_material_score: PropTypes.number,
            ecoscore_shape_ratio: PropTypes.number,
            material: PropTypes.string,
            shape: PropTypes.string
          })
        ),
        score: PropTypes.number
      }),
      production_system: PropTypes.shape({
        labels: PropTypes.array,
        value: PropTypes.number,
        warning: PropTypes.string
      }),
      threatened_species: PropTypes.shape({
        warning: PropTypes.string
      })
    }),
    agribalyse: PropTypes.shape({
      warning: PropTypes.string,
      agribalyse_proxy_food_code: PropTypes.string,
      co2_agriculture: PropTypes.string,
      co2_consumption: PropTypes.string,
      co2_distribution: PropTypes.string,
      co2_packaging: PropTypes.string,
      co2_processing: PropTypes.string,
      co2_total: PropTypes.string,
      co2_transportation: PropTypes.string,
      code: PropTypes.string,
      dqr: PropTypes.string,
      ef_agriculture: PropTypes.string,
      ef_consumption: PropTypes.string,
      ef_distribution: PropTypes.string,
      ef_packaging: PropTypes.string,
      ef_processing: PropTypes.string,
      ef_total: PropTypes.string,
      ef_transportation: PropTypes.string,
      is_beverage: PropTypes.number,
      name_en: PropTypes.string,
      name_fr: PropTypes.string,
      score: PropTypes.number
    }),
    missing: PropTypes.shape({
      agb_category: PropTypes.number,
      ingredients: PropTypes.number,
      labels: PropTypes.number,
      origins: PropTypes.number,
      packagings: PropTypes.number
    }),
    missing_agribalyse_match_warning: PropTypes.number,
    status: PropTypes.string,
    grade: PropTypes.string,
    grade_be: PropTypes.string,
    grade_ch: PropTypes.string,
    grade_de: PropTypes.string,
    grade_es: PropTypes.string,
    grade_fr: PropTypes.string,
    grade_ie: PropTypes.string,
    grade_it: PropTypes.string,
    grade_lu: PropTypes.string,
    grade_nl: PropTypes.string,
    grade_uk: PropTypes.string,
    missing_data_warning: PropTypes.number,
    score: PropTypes.number,
    score_be: PropTypes.number,
    score_ch: PropTypes.number,
    score_de: PropTypes.number,
    score_es: PropTypes.number,
    score_fr: PropTypes.number,
    score_ie: PropTypes.number,
    score_it: PropTypes.number,
    score_lu: PropTypes.number,
    score_nl: PropTypes.number,
    score_uk: PropTypes.number
  }),
  ecoscore_grade: PropTypes.string,
  ecoscore_tags: PropTypes.arrayOf(PropTypes.string),
  editors_tags: PropTypes.arrayOf(PropTypes.string),
  entry_dates_tags: PropTypes.arrayOf(PropTypes.string),
  id: PropTypes.string,
  image_front_small_url: PropTypes.string,
  image_front_thumb_url: PropTypes.string,
  image_front_url: PropTypes.string,
  image_small_url: PropTypes.string,
  image_thumb_url: PropTypes.string,
  image_url: PropTypes.string,
  images: PropTypes.shape({
    "1": PropTypes.shape({
      sizes: PropTypes.shape({
        "100": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        "400": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        full: PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        })
      }),
      uploaded_t: PropTypes.number,
      uploader: PropTypes.string
    }),
    "2": PropTypes.shape({
      sizes: PropTypes.shape({
        "100": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        "400": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        full: PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        })
      }),
      uploaded_t: PropTypes.number,
      uploader: PropTypes.string
    }),
    "3": PropTypes.shape({
      sizes: PropTypes.shape({
        "100": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        "400": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        full: PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        })
      }),
      uploaded_t: PropTypes.number,
      uploader: PropTypes.string
    }),
    "4": PropTypes.shape({
      sizes: PropTypes.shape({
        "100": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        "400": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        full: PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        })
      }),
      uploaded_t: PropTypes.number,
      uploader: PropTypes.string
    }),
    front_de: PropTypes.shape({
      angle: PropTypes.any,
      coordinates_image_size: PropTypes.string,
      geometry: PropTypes.string,
      imgid: PropTypes.string,
      normalize: PropTypes.any,
      rev: PropTypes.string,
      sizes: PropTypes.shape({
        "100": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        "200": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        "400": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        full: PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        })
      }),
      white_magic: PropTypes.any,
      x1: PropTypes.any,
      x2: PropTypes.any,
      y1: PropTypes.any,
      y2: PropTypes.any
    }),
    front_it: PropTypes.shape({
      angle: PropTypes.number,
      coordinates_image_size: PropTypes.string,
      geometry: PropTypes.string,
      imgid: PropTypes.string,
      normalize: PropTypes.any,
      rev: PropTypes.string,
      sizes: PropTypes.shape({
        "100": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        "200": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        "400": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        full: PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        })
      }),
      white_magic: PropTypes.any,
      x1: PropTypes.string,
      x2: PropTypes.string,
      y1: PropTypes.string,
      y2: PropTypes.string
    }),
    ingredients_it: PropTypes.shape({
      angle: PropTypes.any,
      coordinates_image_size: PropTypes.string,
      geometry: PropTypes.string,
      imgid: PropTypes.string,
      normalize: PropTypes.any,
      rev: PropTypes.string,
      sizes: PropTypes.shape({
        "100": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        "200": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        "400": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        full: PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        })
      }),
      white_magic: PropTypes.any,
      x1: PropTypes.any,
      x2: PropTypes.any,
      y1: PropTypes.any,
      y2: PropTypes.any
    }),
    front_es: PropTypes.shape({
      angle: PropTypes.string,
      coordinates_image_size: PropTypes.string,
      geometry: PropTypes.string,
      imgid: PropTypes.string,
      normalize: PropTypes.string,
      rev: PropTypes.string,
      sizes: PropTypes.shape({
        "100": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        "200": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        "400": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        full: PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        })
      }),
      white_magic: PropTypes.string,
      x1: PropTypes.string,
      x2: PropTypes.string,
      y1: PropTypes.string,
      y2: PropTypes.string
    }),
    nutrition_es: PropTypes.shape({
      angle: PropTypes.number,
      coordinates_image_size: PropTypes.string,
      geometry: PropTypes.string,
      imgid: PropTypes.number,
      normalize: PropTypes.any,
      rev: PropTypes.number,
      sizes: PropTypes.shape({
        "100": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        "200": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        "400": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        full: PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        })
      }),
      white_magic: PropTypes.any,
      x1: -PropTypes.number,
      x2: -PropTypes.number,
      y1: -PropTypes.number,
      y2: -PropTypes.number
    }),
    front_fr: PropTypes.shape({
      angle: PropTypes.number,
      coordinates_image_size: PropTypes.string,
      geometry: PropTypes.string,
      imgid: PropTypes.string,
      normalize: PropTypes.any,
      rev: PropTypes.string,
      sizes: PropTypes.shape({
        "100": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        "200": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        "400": PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        }),
        full: PropTypes.shape({
          h: PropTypes.number,
          w: PropTypes.number
        })
      }),
      white_magic: PropTypes.any,
      x1: -PropTypes.number,
      x2: -PropTypes.number,
      y1: -PropTypes.number,
      y2: -PropTypes.number
    })
  }),
  informers_tags: PropTypes.arrayOf(PropTypes.string),
  interface_version_created: PropTypes.string,
  interface_version_modified: PropTypes.string,
  labels: PropTypes.string,
  labels_hierarchy: PropTypes.arrayOf(PropTypes.string),
  labels_lc: PropTypes.string,
  labels_tags: PropTypes.arrayOf(PropTypes.string),
  lang: PropTypes.string,
  languages: PropTypes.shape({
    "en:german": PropTypes.number,
    "en:italian": PropTypes.number,
    "en:spanish": PropTypes.number,
    "en:french": PropTypes.number,
    "en:english": PropTypes.number
  }),
  languages_codes: PropTypes.shape({
    de: PropTypes.number,
    it: PropTypes.number,
    es: PropTypes.number,
    fr: PropTypes.number,
    en: PropTypes.number
  }),
  languages_hierarchy: PropTypes.arrayOf(PropTypes.string),
  languages_tags: PropTypes.arrayOf(PropTypes.string),
  last_edit_dates_tags: PropTypes.arrayOf(PropTypes.string),
  last_editor: PropTypes.string,
  last_image_dates_tags: PropTypes.arrayOf(PropTypes.string),
  last_image_t: PropTypes.number,
  last_modified_by: PropTypes.string,
  last_modified_t: PropTypes.number,
  lc: PropTypes.string,
  main_countries_tags: PropTypes.array,
  max_imgid: PropTypes.string,
  misc_tags: PropTypes.arrayOf(PropTypes.string),
  nova_group_debug: PropTypes.string,
  nova_group_tags: PropTypes.arrayOf(PropTypes.string),
  nutrient_levels: PropTypes.shape({}),
  nutrient_levels_tags: PropTypes.array,
  nutriments: PropTypes.shape({
    alcohol: PropTypes.number,
    alcohol_100g: PropTypes.number,
    alcohol_serving: PropTypes.number,
    alcohol_unit: PropTypes.string,
    alcohol_value: PropTypes.number,
    carbohydrates: PropTypes.number,
    carbohydrates_100g: PropTypes.number,
    carbohydrates_unit: PropTypes.string,
    carbohydrates_value: PropTypes.number,
    energy: PropTypes.number,
    "energy-kcal": PropTypes.number,
    "energy-kcal_100g": PropTypes.number,
    "energy-kcal_unit": PropTypes.string,
    "energy-kcal_value": PropTypes.number,
    energy_100g: PropTypes.number,
    energy_unit: PropTypes.string,
    energy_value: PropTypes.number,
    fat: PropTypes.number,
    fat_100g: PropTypes.number,
    fat_unit: PropTypes.string,
    fat_value: PropTypes.number,
    "fruits-vegetables-nuts-estimate-from-ingredients_100g": PropTypes.number,
    proteins: PropTypes.number,
    proteins_100g: PropTypes.number,
    proteins_unit: PropTypes.string,
    proteins_value: PropTypes.number,
    salt: PropTypes.number,
    salt_100g: PropTypes.number,
    salt_unit: PropTypes.string,
    salt_value: PropTypes.number,
    "saturated-fat": PropTypes.number,
    "saturated-fat_100g": PropTypes.number,
    "saturated-fat_unit": PropTypes.string,
    "saturated-fat_value": PropTypes.number,
    sodium: PropTypes.number,
    sodium_100g: PropTypes.number,
    sodium_unit: PropTypes.string,
    sodium_value: PropTypes.number,
    sugars: PropTypes.number,
    sugars_100g: PropTypes.number,
    sugars_unit: PropTypes.string,
    sugars_value: PropTypes.number,
    "nova-group": PropTypes.number,
    "nova-group_100g": PropTypes.number,
    "nova-group_serving": PropTypes.number
  }),
  nutrition_data_per: PropTypes.string,
  nutrition_data_prepared_per: PropTypes.string,
  nutrition_grades_tags: PropTypes.arrayOf(PropTypes.string),
  nutrition_score_beverage: PropTypes.number,
  nutrition_score_debug: PropTypes.string,
  packaging: PropTypes.string,
  packaging_tags: PropTypes.arrayOf(PropTypes.string),
  packagings: PropTypes.arrayOf(
    PropTypes.shape({
      shape: PropTypes.string
    })
  ),
  photographers_tags: PropTypes.arrayOf(PropTypes.string),
  pnns_groups_1: PropTypes.string,
  pnns_groups_1_tags: PropTypes.arrayOf(PropTypes.string),
  pnns_groups_2: PropTypes.string,
  pnns_groups_2_tags: PropTypes.arrayOf(PropTypes.string),
  popularity_key: PropTypes.number,
  product_name: PropTypes.string,
  product_name_de: PropTypes.string,
  product_quantity: PropTypes.number,
  purchase_places: PropTypes.string,
  purchase_places_tags: PropTypes.arrayOf(PropTypes.string),
  quantity: PropTypes.string,
  removed_countries_tags: PropTypes.array,
  rev: PropTypes.number,
  selected_images: PropTypes.shape({
    front: PropTypes.shape({
      display: PropTypes.shape({
        de: PropTypes.string,
        it: PropTypes.string,
        es: PropTypes.string,
        fr: PropTypes.string
      }),
      small: PropTypes.shape({
        de: PropTypes.string,
        it: PropTypes.string,
        es: PropTypes.string,
        fr: PropTypes.string
      }),
      thumb: PropTypes.shape({
        de: PropTypes.string,
        it: PropTypes.string,
        es: PropTypes.string,
        fr: PropTypes.string
      })
    }),
    ingredients: PropTypes.shape({
      display: PropTypes.shape({
        it: PropTypes.string
      }),
      small: PropTypes.shape({
        it: PropTypes.string
      }),
      thumb: PropTypes.shape({
        it: PropTypes.string
      })
    }),
    nutrition: PropTypes.shape({
      display: PropTypes.shape({
        es: PropTypes.string
      }),
      small: PropTypes.shape({
        es: PropTypes.string
      }),
      thumb: PropTypes.shape({
        es: PropTypes.string
      })
    })
  }),
  states: PropTypes.string,
  states_hierarchy: PropTypes.arrayOf(PropTypes.string),
  states_tags: PropTypes.arrayOf(PropTypes.string),
  stores: PropTypes.string,
  stores_tags: PropTypes.arrayOf(PropTypes.string),
  traces: PropTypes.string,
  traces_from_ingredients: PropTypes.string,
  traces_from_user: PropTypes.string,
  traces_hierarchy: PropTypes.array,
  traces_tags: PropTypes.array,
  unknown_nutrients_tags: PropTypes.array,
  url: PropTypes.string,
  additives_n: PropTypes.number,
  additives_old_n: PropTypes.number,
  additives_old_tags: PropTypes.array,
  additives_original_tags: PropTypes.array,
  additives_tags: PropTypes.array,
  amino_acids_tags: PropTypes.array,
  categories_old: PropTypes.string,
  cities_tags: PropTypes.array,
  countries_lc: PropTypes.string,
  data_sources: PropTypes.string,
  data_sources_tags: PropTypes.arrayOf(PropTypes.string),
  downgraded: PropTypes.string,
  ecoscore_score: PropTypes.number,
  emb_codes: PropTypes.string,
  emb_codes_tags: PropTypes.array,
  image_ingredients_small_url: PropTypes.string,
  image_ingredients_thumb_url: PropTypes.string,
  image_ingredients_url: PropTypes.string,
  ingredients: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      percent: PropTypes.number,
      percent_estimate: PropTypes.number,
      percent_max: PropTypes.number,
      percent_min: PropTypes.number,
      text: PropTypes.string
    })
  ),
  ingredients_analysis_tags: PropTypes.arrayOf(PropTypes.string),
  ingredients_from_or_that_may_be_from_palm_oil_n: PropTypes.number,
  ingredients_from_palm_oil_n: PropTypes.number,
  ingredients_from_palm_oil_tags: PropTypes.array,
  ingredients_hierarchy: PropTypes.arrayOf(PropTypes.string),
  ingredients_n: PropTypes.number,
  ingredients_n_tags: PropTypes.arrayOf(PropTypes.string),
  ingredients_original_tags: PropTypes.arrayOf(PropTypes.string),
  ingredients_tags: PropTypes.arrayOf(PropTypes.string),
  ingredients_text: PropTypes.string,
  ingredients_text_it: PropTypes.string,
  ingredients_text_with_allergens: PropTypes.string,
  ingredients_text_with_allergens_it: PropTypes.string,
  ingredients_that_may_be_from_palm_oil_n: PropTypes.number,
  ingredients_that_may_be_from_palm_oil_tags: PropTypes.array,
  ingredients_with_specified_percent_n: PropTypes.number,
  ingredients_with_specified_percent_sum: PropTypes.number,
  ingredients_with_unspecified_percent_n: PropTypes.number,
  ingredients_with_unspecified_percent_sum: PropTypes.number,
  known_ingredients_n: PropTypes.number,
  link: PropTypes.string,
  manufacturing_places: PropTypes.string,
  manufacturing_places_tags: PropTypes.arrayOf(PropTypes.string),
  minerals_tags: PropTypes.array,
  nucleotides_tags: PropTypes.array,
  origins: PropTypes.string,
  origins_hierarchy: PropTypes.arrayOf(PropTypes.string),
  origins_lc: PropTypes.string,
  origins_tags: PropTypes.arrayOf(PropTypes.string),
  other_nutritional_substances_tags: PropTypes.array,
  product_name_it: PropTypes.string,
  traces_lc: PropTypes.string,
  unknown_ingredients_n: PropTypes.number,
  update_key: PropTypes.string,
  vitamins_tags: PropTypes.array,
  allergens_lc: PropTypes.string,
  category_properties: PropTypes.shape({
    "ciqual_food_name:en": PropTypes.string,
    "ciqual_food_name:fr": PropTypes.string
  }),
  ciqual_food_name_tags: PropTypes.arrayOf(PropTypes.string),
  debug_param_sorted_langs: PropTypes.arrayOf(PropTypes.string),
  expiration_date: PropTypes.string,
  generic_name: PropTypes.string,
  generic_name_es: PropTypes.string,
  image_nutrition_small_url: PropTypes.string,
  image_nutrition_thumb_url: PropTypes.string,
  image_nutrition_url: PropTypes.string,
  ingredients_text_es: PropTypes.string,
  ingredients_text_with_allergens_es: PropTypes.string,
  no_nutrition_data: PropTypes.string,
  nutrition_data: PropTypes.string,
  nutrition_data_prepared: PropTypes.string,
  packaging_text: PropTypes.string,
  packaging_text_es: PropTypes.string,
  popularity_tags: PropTypes.arrayOf(PropTypes.string),
  product_name_es: PropTypes.string,
  scans_n: PropTypes.number,
  sortkey: PropTypes.number,
  unique_scans_n: PropTypes.number,
  origin: PropTypes.string,
  origin_es: PropTypes.string,
  origins_old: PropTypes.string,
  additives_debug_tags: PropTypes.array,
  additives_prev_original_tags: PropTypes.array,
  amino_acids_prev_tags: PropTypes.array,
  generic_name_fr: PropTypes.string,
  ingredients_debug: PropTypes.arrayOf(PropTypes.string),
  ingredients_ids_debug: PropTypes.arrayOf(PropTypes.string),
  ingredients_text_debug: PropTypes.any,
  ingredients_text_fr: PropTypes.string,
  ingredients_text_with_allergens_fr: PropTypes.string,
  minerals_prev_tags: PropTypes.array,
  nova_group: PropTypes.number,
  nova_groups: PropTypes.string,
  nova_groups_tags: PropTypes.arrayOf(PropTypes.string),
  nucleotides_prev_tags: PropTypes.array,
  origin_fr: PropTypes.string,
  packaging_text_fr: PropTypes.string,
  product_name_fr: PropTypes.string,
  sources: PropTypes.arrayOf(
    PropTypes.shape({
      fields: PropTypes.arrayOf(PropTypes.string),
      id: PropTypes.string,
      images: PropTypes.arrayOf(PropTypes.string),
      import_t: PropTypes.number,
      url: PropTypes.string
    })
  ),
  vitamins_prev_tags: PropTypes.array,
  generic_name_it: PropTypes.string,
  origin_it: PropTypes.string,
  packaging_text_it: PropTypes.string,
  generic_name_de: PropTypes.string,
  generic_name_en: PropTypes.string,
  ingredients_text_de: PropTypes.string,
  ingredients_text_en: PropTypes.string,
  ingredients_text_with_allergens_de: PropTypes.string,
  ingredients_text_with_allergens_en: PropTypes.string,
  origin_de: PropTypes.string,
  origin_en: PropTypes.string,
  packaging_text_de: PropTypes.string,
  packaging_text_en: PropTypes.string,
  product_name_en: PropTypes.string
}
